# [ebay_gsp_filter](https://chrome.google.com/webstore/detail/ebay-gsp-filter/elgbjimnmkhnpbliadkgcpogfgncohin)
Filters out all items that utilise the "Global Shipping Program(GSP)"
This is a really small chrome extension that filters out items that are handled by Ebay's "Global shipping program".

The only reason this extension even exists is because Ebay don't seem to want to add such a filter by themselves
if you also got tired of seeing items with so called "import charges" that cost more than the item, then this is the extension for you.
