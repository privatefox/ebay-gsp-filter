const maxDepth = 8;
const gspItemClass = 'epli';
const resultCountClass = 'rsHdr';
const generalItemClass = 'sresult';
const hideDisplayValue = "none";
const showDisplayValue = "block";

function getGspItems() {
    var gspItems = document.getElementsByClassName(gspItemClass);
    return gspItems;
}

function modifyGspItems() {
    var GspItems = getGspItems();
    var revertCheckBox = document.getElementById('gspResultsCheckBox');
    if (revertCheckBox.checked == true) {
        displayValue = "none";
    }
    else {
        displayValue = "block";
    }
    gspItemCount = gspItems.length;
    for (var item_number = 0; item_number < gspItems.length; item_number++) {
        parentItem = gspItems[item_number].parentNode;
        for (var currentDepth = 0; currentDepth < maxDepth; currentDepth++) {
            parentItem = parentItem.parentNode;
            if (parentItem.classList.contains(generalItemClass)) {
                parentItem.style.display = displayValue;
                break;
            }
        }
        
    }
}

function printHideResult(gspItemCount) {
    var ResultCountItems = document.getElementsByClassName(resultCountClass)
    resultStr = ResultCountItems[0];
    resultStr.innerText += ' | hide ' + gspItemCount + ' GSP items: ';
    addRevertButton(resultStr);
}

function addRevertButton(parentElement) {
    var revertCheckBox = document.createElement('input');
    revertCheckBox.type = 'checkbox';
    revertCheckBox.id = "gspResultsCheckBox";
    revertCheckBox.addEventListener("change", modifyGspItems);
    revertCheckBox.checked = true;
    parentElement.appendChild(revertCheckBox)
}

gspItems = getGspItems();
printHideResult(gspItems.length);
modifyGspItems();